#!/bin/bash

set -eu -o pipefail

# service HOST:PORT
: $DESTINATION

# k8s resource to scale
: $RESOURCE

# labels of the pods that run this script, used to ensure it is the only replica before scaling to zero
: $SELF_LABELS

# your service must become available within X seconds
: ${START_TIMEOUT=300}

# wait for X seconds after the last connection closed before scaling to zero
: ${STOP_AFTER=1800}

# TCP port to listen on
: ${PORT=8080}

# "really big number", practically unlimited for most use cases but won't overflow anything
: ${MAXCONN=10000}

# file to use for advisory locks
: ${LOCKFILE=/bin/socat}

# set up socat and attach to our stdin
exec < <(socat -dd tcp-listen:$PORT,reuseaddr,fork,max-children=$MAXCONN,nodelay \
	EXEC:"flock -s \"$LOCKFILE\" socat - \"tcp:$DESTINATION,retry=$START_TIMEOUT,nodelay\"" 2>&1)

# state
replicas=unknown
terminate=false

has_connections () {
	! flock -xn "$LOCKFILE" true
}

scale () {
	new_replicas=$1
	if [[ $new_replicas != $replicas ]]
	then
		if [[ $new_replicas -eq 0 && $(kubectl get pod --selector "$SELF_LABELS" -ogo-template='{{len .items}}') -ne 1 ]]
		then
			echo "not scaling to zero because we're not the only replica"
			return
		fi
		echo scaling to $new_replicas

		local locker=""
		if [[ $new_replicas == 0 ]]
		then
			locker="flock -xn $(printf %q "$LOCKFILE")"
		fi

		if $locker kubectl scale $RESOURCE --replicas $new_replicas --timeout 60s
		then
			replicas=$new_replicas
		fi
	fi
}

handle_sigterm () {
	terminate=true
	echo stopping...
	if ! has_connections
	then
		exit 143
	else
		echo waiting for open connections to close
	fi
}
trap handle_sigterm TERM

echo started

until $terminate && ! has_connections
do
	if read -t $STOP_AFTER
	then
		if has_connections
		then
			scale 1
		fi
	elif ! has_connections # no log message within $START_TIMEOUT and no open connections
	then
		scale 0
	fi
done

exit 143
