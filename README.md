# hxzeroscale

simple scale-to-zero for non-HA k8s workloads

## Quickstart

Install with a single command, replace everything named `some-app` with the appropriate values for your app.
Then adapt your ingress / service settings.

```sh
helm upgrade --install \
some-app \
hxzeroscale --repo https://gitlab.com/api/v4/projects/45479547/packages/helm/main \
--version 1.0.1 \
--set target.service.hostname=some-app-service \
--set target.service.port=80 \
--set target.resource.name=some-app-deployment
```

The output will show the most important information:

```
[...]
NOTES:
service:  some-app-hxzeroscale
target:   some-app-service:80
resource: Deployment/some-app-deployment
startTimeout: 300
stopAfter:    1800
```

## What's this?

Some Kubernetes deployments aren't used very often and just take up resources.
If a slight delay is acceptable for the first connection after a long period of idling one can automatically scale such workloads to zero and scale them back up when they are accessed.

There are various projects that solve this problem already, this one focuses on:

- **Simplicity**: It's just [a couple of lines of `bash`](./hxzeroscale.sh) that manage a [`socat`](http://www.dest-unreach.org/socat/) process with [tests](./test/run.sh). No compilation, no CRDs, no globally running operator.
- **Arbitrary TCP support**: You can use this with anything that speaks TCP, be it HTTP, unterminated TLS, or a custom protocol.
- **Easy Kubernetes deployment**: Deployment through a Helm chart with the script baked right into the manifest. No persistent storage required. Probes, RBAC configuration, etc. included.
- **Low resource requirements**: While you have to run one instance per managed workload, it only needs <4MiB of memory.

It is likely unsuitable for:

- **High Availablity**: Only a single replica is supported and all traffic is proxied.
- **Maximum Performance**: While efficiently proxying TCP traffic within a cluster should usually not be a big problem, it is still an extra hop.

### How does it work?

```
+---------------------------------------+
| ingress controller / external clients |
+---------------------------------------+
  |
  | TCP connections
  V
+-------------+
| hxzeroscale |
+-------------+
  |
  | TCP connections,
  | scale workload to 0 and 1 as needed
  V
+-----+
| app |
+-----+
```

1. The `bash` script starts `socat` which proxies all TCP traffic.
1. Once `socat` reports the first connection the workload is scaled up to 1.
1. `socat` keeps retrying the proxied service and once successful forwards the connection from the client. The first connection is just stalled for a bit, not dropped.
1. The `bash` script keeps an eye on the number of open connections, once they reach and stay at zero for the configured period of time the workload is scaled to 0 again. New connections that are opened while scaling to zero are held until the workload is scaled to 1 again.

## Details

### Use with ready-made packages

If you want to use this with a ready-made package distributed via Helm, kustomize, etc. make sure it does not reset `replicas` on every update and point ingresses / services to the proxy service instead.
How that can be done depends on the tools you use.

### Graceful restarts

When the proxy deployment is updated or restarted, the old replica is kept around until its last connection has been closed while the new replica handles new connections.
The maximum duration to keep a replica running for existing connections can be configured through `terminationGracePeriodSeconds` in the Helm values.

As long as there are multiple replicas, each one assumes the others might have open connections and will not scale the proxied workload to 0.

### Resources and scheduling

The low resource requests of the proxy should allow the Kubernetes scheduler to use resources for other workloads until the proxied workload is accessed and thereby scaled up.

### Custom images

You can use any custom image as long as it has similar versions of:

- `bash`
- `socat`
- `kubectl`
- `busybox` or the tools used therefrom

By default a suitable image is assembled through [Nixery](https://nixery.dev/).

### Replicas and deployment automation

The workloads are scaled as needed.
Make sure not to set `replicas` explicitly through your deployment tool,
otherwise unused workloads might not be scaled to zero or connections are stalled until they time out (for `replicas: 0`).

### Nagle's algorithm / TCP_NODELAY

Nagle's algorithm is disabled as it is assumed that client and server send sensible packets.

## Contributing

### Bugs

Browse and report bugs here: [gitlab.com/hxtmdev/hxzeroscale/-/issues](https://gitlab.com/hxtmdev/hxzeroscale/-/issues)

### Small changes

If you have a small improvement or bugfix that does not add much complexity or otherwise interferes with the project goals just open a merge request here: [gitlab.com/hxtmdev/hxzeroscale/-/merge_requests](https://gitlab.com/hxtmdev/hxzeroscale/-/merge_requests)

### Big changes

If you intend to make bigger changes or spend lots of time on something _and want it integrated into this repository_, please open an issue first to discuss it.
Of course, you have the option to maintain your own fork in line with the [license](./LICENSE).

### Commit requirements

All commits require a [DCO](https://developercertificate.org/) sign-off, which can be generated automatically by [calling `git commit` with `--signoff`](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff).

Please include a body in every commit message with any details you think can help others understand what you did and why.
