#!/bin/bash

# these tests mock kubectl and socat,
# fake a few socat log messages
# and check the kubectl invocations as well as log output

# prefix script invocation with `DEBUG= ` to see what went wrong
if [[ -v DEBUG ]]
then
	set -x
fi

set -eu -o pipefail

cd "$(dirname "$0")"

: ${TIME_MULTIPLIER=1}

export DESTINATION=localhost:1234
export RESOURCE=deployment/foobar
export SELF_LABELS=somename
export STOP_AFTER=$(( 3 * $TIME_MULTIPLIER ))

sleep_for () {
	sleep $(( $1 * $TIME_MULTIPLIER ))
}

cleanup () {
	if [[ -v PID ]]
	then
		kill -9 $PID &>/dev/null || true
	fi
	rm -v {stdout,stderr,kubectl,socat}.log socat.{stdout,connections} &>/dev/null || true
}
trap cleanup EXIT

start_script () {
	cleanup
	PATH=$PWD/mocks:$PATH ../hxzeroscale.sh >stdout.log 2>stderr.log &
	PID=$!
}

logs () {
	local logfile=$1.log
	if [[ -e "$logfile" ]]
	then
		tr -d \\0 < "$logfile"
		truncate -s0 "$logfile"
	fi
}

set_connections () {
	echo $1 >socat.connections
	echo >>socat.stdout some line
}

script_running () {
	[[ -e /proc/$PID ]]
}

script_stopped () {
	! script_running
}

start_script

echo only startup message logged initially
sleep_for 2
[[ `logs stdout` == started ]]
[[ `logs kubectl` == "" ]]

echo scale from unknown to 0 after \$STOP_AFTER seconds
sleep_for 2
[[ `logs stdout` == $'scaling to 0\noutput from kubectl scale deployment/foobar --replicas 0 --timeout 60s' ]]
[[ `logs kubectl` == $'get pod --selector somename -ogo-template={{len .items}}\nscale deployment/foobar --replicas 0 --timeout 60s' ]]

echo scale to 1 on connection
set_connections 1
sleep_for 1
[[ `logs stdout` == $'scaling to 1\noutput from kubectl scale deployment/foobar --replicas 1 --timeout 60s' ]]
[[ `logs kubectl` == "scale deployment/foobar --replicas 1 --timeout 60s" ]]

echo new connections, closed connections, waiting do not change a thing
set_connections 2
set_connections 1
sleep_for 4
[[ `logs stdout` == "" ]]
[[ `logs kubectl` == "" ]]

echo wait for \$STOP_AFTER seconds after the last connection closes
set_connections 0
sleep_for 2
[[ `logs stdout` == "" ]]
[[ `logs kubectl` == "" ]]

echo then scale down
sleep_for 2
[[ `logs stdout` == $'scaling to 0\noutput from kubectl scale deployment/foobar --replicas 0 --timeout 60s' ]]
[[ `logs kubectl` == $'get pod --selector somename -ogo-template={{len .items}}\nscale deployment/foobar --replicas 0 --timeout 60s' ]]

echo without open connections, exit immediately on SIGTERM
set_connections 1
set_connections 0
sleep_for 1
: reset `logs stdout`
kill $PID
sleep_for 1
script_stopped
[[ `logs stdout` == $'stopping...' ]]

echo with open connections, exit after the last one closes
start_script
sleep_for 1
set_connections 1
sleep_for 1
: reset `logs stdout`
kill $PID
sleep_for 1
set_connections 2
set_connections 1
sleep_for 1
script_running
set_connections 0
sleep_for 1
script_stopped
[[ `logs stdout` == $'stopping...\nwaiting for open connections to close' ]]

echo "will not scale to zero if not the only replica (since another replica might have open connections too)"
SELF_LABELS=self start_script
sleep_for 1
: reset `logs stdout`
sleep_for 3
[[ `logs stdout` == "not scaling to zero because we're not the only replica" ]]
kill $PID
wait

echo nothing on stderr during all of testing
[[ `logs stderr` == "" ]]
